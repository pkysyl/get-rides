import { Ride } from "@/models/Ride.model";
import axios, { AxiosPromise } from "axios";

export function fetchRides(): Promise<Ride[]> {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve([]);
    }, 300);
  });
}

export class RideService {
  private endpoint: string;
  private entity: string;

  constructor(endpoint: string, entity: string) {
    this.endpoint = endpoint;
    this.entity = entity;
  }

  public getAllRequest(): AxiosPromise<{ rides: Ride[] }> {
    const response = axios.get(`${this.endpoint}${this.entity}`);
    return response;
  }

  public createRide(data: Ride): AxiosPromise<{ ride: Ride }> {
    return axios.post(`${this.endpoint}${this.entity}`, { ride: data });
  }

  public getPriceForRide(rideId: number): AxiosPromise<number> {
    return axios.get(`${this.endpoint}${this.entity}/${rideId}/price`);
  }
}
