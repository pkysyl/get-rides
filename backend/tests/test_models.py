 
from datetime import datetime

from rideApi.models import Ride

def test_new_ride():
    """
    GIVEN a Ride model
    WHEN a new Ride is created
    THEN check the duration, start_date, distance are defined correctly
    """
    datetime_str = '09/19/18 13:55:26'
    datetime_object = datetime.strptime(datetime_str, '%m/%d/%y %H:%M:%S')
    ride = Ride(distance = 1, duration = 3, start_date =datetime_object)
    assert ride.distance == 1
    assert ride.duration == 3
    assert ride.start_date.date() == datetime_object.date()

