import { Ride } from "../Ride.model";

export interface RideState {
  rides: Ride[];
}

export interface RideContext {
  rides: Ride[];
}
