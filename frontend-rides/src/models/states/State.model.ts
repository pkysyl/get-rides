import { Ride } from "../Ride.model";

export interface State {
  rides: Ride[];
}
