# flask-rides-backend

Example of a simple flask rest api project. 
This project is ready to use.

## Requirements
python version >=3.4

## Getting started

Otherwise, for the standalone web service:

```shell
pip install Flask Flask-SQLAlchemy Flask-Migrate Flask-Script Flask-CORS requests
python manage.py runserver
```

Visit [http://localhost:5000](http://localhost:5000)

## Development

## Tests

Standalone unit tests run with:

```shell
pip install pytest pytest-cov pytest-flask
python -m pytest tests/
```
