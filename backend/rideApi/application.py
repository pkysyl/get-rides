"""
application.py
- creates a Flask app instance and registers the database object
"""

from flask import Flask
from flask_cors import CORS

def create_app(app_name='RIDE_API'):
    app = Flask(app_name)
    cors = CORS(app, resources={r"/*": {"origins": "*"}})
    app.config.from_object('rideApi.config.BaseConfig')
    app.config['CORS_HEADERS'] = 'Content-Type'


    from rideApi.api import api
    app.register_blueprint(api, url_prefix="/api")

    from rideApi.models import db
    db.init_app(app)
    return app