from datetime import timedelta
import math
def getPrice(distance, duration, startDate):
    # calculate price without the extras optionals
    priceForMiles = 1 + distance * 5 * 0.5

    # we calculate the extra and use a trick to have a price with a precision of 0.5
    calculatedPrice = calculateExtraRushHour(startDate, duration) + calculateExtraNight(startDate, duration) + priceForMiles
    return math.floor(calculatedPrice * 2)/2


def calculateExtraNight(startDate, duration):
    startHour = startDate.hour
    if (startHour >= 20 or startHour < 6):
        return 0.5
    secondsToStartExtra = calculateSecondsToStartExtra(startDate, 20)

    # if the duration is bigger that the time to enter the next extra zone we add the extra
    if (duration > secondsToStartExtra): 
        return 0.5
    return 0

def calculateExtraRushHour(startDate, duration):
    startHour = startDate.hour
    if (startHour >= 16 and startHour < 19):
        return 1
    secondsToStartExtra = calculateSecondsToStartExtra(startDate, 16)

    # if the duration is bigger that the time to enter the next extra zone we add the extra
    if (duration > secondsToStartExtra): 
        return 1
    return 0

def calculateSecondsToStartExtra(starDate, startExtraHour):
    # In that cas we are before the starthour of the extra perdiod in the same day
    if starDate.hour < startExtraHour: 
        return (starDate.replace(hour=startExtraHour, minute=0, second=0, microsecond=0) - starDate).total_seconds()
    # In that case we are after the end of the extra period
    return 24 * 60 * 60 - (starDate - starDate.replace(hour=startExtraHour, minute=0, second=0, microsecond=0)).total_seconds()
