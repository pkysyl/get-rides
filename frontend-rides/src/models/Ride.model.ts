export interface Ride {
  id: number;
  distance: string;
  startDate: Date;
  duration: number;
  price?: number;
}
