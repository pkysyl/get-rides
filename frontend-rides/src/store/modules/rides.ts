import {
  VuexModule,
  Module,
  getModule,
  Mutation,
  Action,
} from "vuex-module-decorators";
import Vue from "vue";
import store from "@/store";
import { fetchRides, RideService } from "@/services";
import { Ride } from "@/models/Ride.model";

/**
 * When namespaced is set to true, contents of the module is automatically namedspaced based on name.
 * Vuex allows adding modules to store dynamically after store is created, we set it to true.
 * store is the Vuex store to which we need to insert our module.
 */
@Module({
  namespaced: true,
  name: "rides",
  store,
  dynamic: true,
})
class RideModule extends VuexModule {
  /**
   * Class variables automatically become part of the state.
   * Our module thus will have products/allProducts and products/service as state variables.
   * The reason we put service inside Vuex is because in this case there will be only one ProductService
   * which can be shared between all components.
   */
  public allRides: Ride[] = [];
  public service: RideService = new RideService(
    "http://127.0.0.1:5000/api/",
    "rides"
  );

  // Action automatically calls setRides function with arguments returned by fetchRides function.
  @Action({ commit: "setRides" })
  public async fetchAllRides() {
    // Calls into service to get all Rides
    const rid = await this.service.getAllRequest();
    return rid.data.rides;
  }

  // Action automatically calls addRide function with arguments returned by fetchProducts function.
  @Action({ commit: "addRide" })
  public async pushNewRide(ride: Ride) {
    const rid = await this.service.createRide(ride);
    return rid;
  }

  // Action automatically calls addRide function with arguments returned by fetchProducts function.
  @Action({ commit: "updateRidePrice" })
  public async findPriceForRide(ride: Ride) {
    const res = await this.service.getPriceForRide(ride.id);
    console.log("res", res);
    ride.price = res.data;
    return ride;
  }

  // modifies our module state, by setting allProducts to p.
  @Mutation
  public setRides(p: Ride[]) {
    this.allRides = p;
  }

  // modifies our module state, by setting allProducts to p.
  @Mutation
  public addRide(p: Ride) {
    this.allRides.unshift(p);
  }

  // modifies our module state, by setting allProducts to p.
  @Mutation
  public updateRidePrice(ride: Ride) {
    let index = -1;
    this.allRides.find((r, i) => {
      if (r.id === ride.id) {
        index = i;
      }
    });
    if (index >= 0) {
      Vue.set(this.allRides, index, ride);
    }
  }
}

// Export our module so our components can use it.
export default getModule(RideModule);
