"""
api.py
- provides the API endpoints for consuming and producing
  REST requests and responses
"""

from flask import Blueprint, jsonify, request
from rideApi.models import Ride
from rideApi.utils import getPrice
from flask_cors import cross_origin
from datetime import datetime

api = Blueprint('api', __name__)


@api.route('/rides/<id>/price/')
@cross_origin()
def get_price(id):
    ride = Ride.query.get(int(id))
    return jsonify(getPrice(ride.distance, ride.duration, ride.start_date)), 201

@api.route('/rides/', methods=('GET', 'POST'))
@cross_origin()
def fetch_rides():
    if request.method == 'GET':
        rides = Ride.query.all()
        return jsonify({ 'rides': [s.to_dict() for s in rides] })
    elif request.method == 'POST':
        data = request.get_json()
        ride = Ride(startDate=data['startDate'], duration=data['duration'], distance=data['distance'])
        db.session.add(ride)
        db.session.commit()
        return jsonify(ride.to_dict()), 201