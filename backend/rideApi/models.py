"""
models.py
- Data classes for the rideApi application
"""

from datetime import datetime
from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()

class Ride(db.Model):
    __tablename__ = 'rides'

    id = db.Column(db.Integer, primary_key=True)
    start_date = db.Column(db.DateTime, default=datetime.utcnow)
    duration = db.Column(db.Integer)
    distance = db.Column(db.Integer)

    def to_dict(self):
        return dict(id=self.id,
                    startDate=self.start_date.strftime('%Y-%m-%d %H:%M:%S'),
                    duration=self.duration,
                    distance=self.distance)
