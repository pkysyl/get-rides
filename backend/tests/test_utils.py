 
from datetime import datetime

from rideApi.utils import getPrice, calculateExtraRushHour, calculateExtraNight, calculateSecondsToStartExtra

def test_get_price_during_night():
    """
    GIVEN a start_date, duration, distance
    WHEN we get the price of a ride
    THEN check the result in simple cases are ok are defined correctly
    """
    datetime_str = '01/01/20 23:59:59'
    start_date = datetime.strptime(datetime_str, '%m/%d/%y %H:%M:%S')
    duration = 60*60*7
    distance = 1
    extraRushHour = calculateExtraRushHour(start_date, duration)
    extraNight = calculateExtraNight(start_date, duration)
    price = getPrice(distance, duration, start_date)
    assert extraNight == 0.5
    assert extraRushHour == 0
    assert price == 4

def test_get_price_25_hours():
    """
    GIVEN a start_date, duration, distance
    WHEN we get the price of a ride
    THEN check the result in simple cases are ok are defined correctly
    """
    datetime_str = '01/01/20 12:00:00'
    start_date = datetime.strptime(datetime_str, '%m/%d/%y %H:%M:%S')
    duration = 60*60*25
    distance = 10
    extraRushHour = calculateExtraRushHour(start_date, duration)
    extraNight = calculateExtraNight(start_date, duration)
    price = getPrice(distance, duration, start_date)
    assert extraNight == 0.5
    assert extraRushHour == 1
    assert price == 27.5

def test_get_price_casual():
    """
    GIVEN a start_date, duration, distance
    WHEN we get the price of a ride
    THEN check the result in simple cases are ok are defined correctly
    """
    datetime_str = '01/01/20 12:34:08'
    start_date = datetime.strptime(datetime_str, '%m/%d/%y %H:%M:%S')
    duration = 60*60*0.5
    distance = 3.9
    extraRushHour = calculateExtraRushHour(start_date, duration)
    extraNight = calculateExtraNight(start_date, duration)
    price = getPrice(distance, duration, start_date)
    assert extraNight == 0
    assert extraRushHour == 0
    assert price == 10.5

def test_get_price_rush_hour_last_minute():
    """
    GIVEN a start_date, duration, distance
    WHEN we get the price of a ride
    THEN check the result in simple cases are ok are defined correctly
    """
    datetime_str = '01/01/20 15:00:00'
    start_date = datetime.strptime(datetime_str, '%m/%d/%y %H:%M:%S')
    duration = 60*60*1 + 1
    distance = 2
    extraRushHour = calculateExtraRushHour(start_date, duration)
    extraNight = calculateExtraNight(start_date, duration)
    price = getPrice(distance, duration, start_date)
    assert extraNight == 0
    assert extraRushHour == 1
    assert price == 7
